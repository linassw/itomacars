﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ItomaCars.Models;

namespace ItomaCars.Controllers
{
    public class CarManagementsController : Controller
    {
        private CarDBContext db = new CarDBContext();

        // GET: CarManagements
        public ActionResult Index()
        {
            var carManagements = db.CarManagements.Include(c => c.Car).Include(c => c.Segment).Include(c => c.User);
            return View(carManagements.ToList());
        }

        // GET: CarManagements/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarManagement carManagement = db.CarManagements.Find(id);
            if (carManagement == null)
            {
                return HttpNotFound();
            }
            return View(carManagement);
        }

        // GET: CarManagements/Create
        public ActionResult Create()
        {
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number");
            ViewBag.SegmentID = new SelectList(db.Segments, "ID", "Name");
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name");
            return View();
        }

        // POST: CarManagements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,DateFrom,DateTo,UserID,SegmentID,CarID")] CarManagement carManagement)
        {
            if (ModelState.IsValid)
            {
                db.CarManagements.Add(carManagement);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carManagement.CarID);
            ViewBag.SegmentID = new SelectList(db.Segments, "ID", "Name", carManagement.SegmentID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", carManagement.UserID);
            return View(carManagement);
        }

        // GET: CarManagements/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarManagement carManagement = db.CarManagements.Find(id);
            if (carManagement == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carManagement.CarID);
            ViewBag.SegmentID = new SelectList(db.Segments, "ID", "Name", carManagement.SegmentID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", carManagement.UserID);
            return View(carManagement);
        }

        // POST: CarManagements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,DateFrom,DateTo,UserID,SegmentID,CarID")] CarManagement carManagement)
        {
            if (ModelState.IsValid)
            {
                db.Entry(carManagement).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carManagement.CarID);
            ViewBag.SegmentID = new SelectList(db.Segments, "ID", "Name", carManagement.SegmentID);
            ViewBag.UserID = new SelectList(db.Users, "ID", "Name", carManagement.UserID);
            return View(carManagement);
        }

        // GET: CarManagements/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarManagement carManagement = db.CarManagements.Find(id);
            if (carManagement == null)
            {
                return HttpNotFound();
            }
            return View(carManagement);
        }

        // POST: CarManagements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarManagement carManagement = db.CarManagements.Find(id);
            db.CarManagements.Remove(carManagement);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
