﻿using System.Web.Mvc;
using ItomaCars.Models;
using System.Linq;
using System;

namespace ItomaCars.Controllers
{
    public class HomeController : Controller
    {
        private CarDBContext db = new CarDBContext();

        public ActionResult Index()
        {
            var data = from c in db.Cars
                       join ccm in db.CarManagements on c.ID equals ccm.CarID
                       join cu in db.Users on ccm.UserID equals cu.ID
                       join cs in db.Segments on cu.SegmentID equals cs.ID

                       where ccm.DateFrom < DateTime.Now
                       where (ccm.DateTo > DateTime.Now || ccm.DateTo == null)

                       let carStatus = db.CarStatus.Where(pc2 => c.ID == pc2.CarID).Where(p2 => ccm.DateTo >= DateTime.Now).FirstOrDefault()
                       let pcm = db.CarManagements.Where(p2 => c.ID == p2.CarID).Where(p2 => ccm.DateFrom >= p2.DateTo).FirstOrDefault()
                       join pu in db.Users on pcm.UserID equals pu.ID into prevUsers

                       from prevU in prevUsers.DefaultIfEmpty()
                       join ps in db.Segments on prevU.SegmentID equals ps.ID into prevSegments

                       from prevS in prevSegments.DefaultIfEmpty()

                       select new CarDataView()
                       {
                           ID = ccm.ID,
                           CarYearMade = c.YearMade,
                           CarModel = c.Model,
                           CarNumber = c.Number,
                           CurrentUserName = cu.Name,
                           CurrentSegment = cs.Name,
                           Status = carStatus.Status.Name,
                           PreviousUserName = prevU.Name,
                           PreviousSegment = prevS.Name
                       };

            return View(data.ToList());
        }
    }
}