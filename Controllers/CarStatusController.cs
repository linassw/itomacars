﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ItomaCars.Models;

namespace ItomaCars.Controllers
{
    public class CarStatusController : Controller
    {
        private CarDBContext db = new CarDBContext();

        // GET: CarStatus
        public ActionResult Index()
        {
            var carStatus = db.CarStatus.Include(c => c.Car).Include(c => c.Status);
            return View(carStatus.ToList());
        }

        // GET: CarStatus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarStatus carStatus = db.CarStatus.Find(id);
            if (carStatus == null)
            {
                return HttpNotFound();
            }
            return View(carStatus);
        }

        // GET: CarStatus/Create
        public ActionResult Create()
        {
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number");
            ViewBag.StatusID = new SelectList(db.Status, "ID", "Name");
            return View();
        }

        // POST: CarStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,StatusID,CarID,DateFrom,DateTo")] CarStatus carStatus)
        {
            if (ModelState.IsValid)
            {
                db.CarStatus.Add(carStatus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carStatus.CarID);
            ViewBag.StatusID = new SelectList(db.Status, "ID", "Name", carStatus.StatusID);
            return View(carStatus);
        }

        // GET: CarStatus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarStatus carStatus = db.CarStatus.Find(id);
            if (carStatus == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carStatus.CarID);
            ViewBag.StatusID = new SelectList(db.Status, "ID", "Name", carStatus.StatusID);
            return View(carStatus);
        }

        // POST: CarStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,StatusID,CarID,DateFrom,DateTo")] CarStatus carStatus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(carStatus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CarID = new SelectList(db.Cars, "ID", "Number", carStatus.CarID);
            ViewBag.StatusID = new SelectList(db.Status, "ID", "Name", carStatus.StatusID);
            return View(carStatus);
        }

        // GET: CarStatus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CarStatus carStatus = db.CarStatus.Find(id);
            if (carStatus == null)
            {
                return HttpNotFound();
            }
            return View(carStatus);
        }

        // POST: CarStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarStatus carStatus = db.CarStatus.Find(id);
            db.CarStatus.Remove(carStatus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
