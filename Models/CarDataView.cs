﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ItomaCars.Models
{
    public class CarDataView
    {
        public int ID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy}")]
        [Display(Name = "Year")]
        public DateTime CarYearMade { get; set; }

        [Display(Name = "Number")]
        public string CarNumber { get; set; }

        [Display(Name = "Model")]
        public string CarModel { get; set; }

        [Display(Name = "Current user")]
        public string CurrentUserName { get; set; }

        [Display(Name = "Current segment")]
        public string CurrentSegment { get; set; }

        [Display(Name = "Current status")]
        public string Status { get; set; }

        [Display(Name = "Previous user")]
        public string PreviousUserName { get; set; }

        [Display(Name = "Previous segment")]
        public string PreviousSegment { get; set; }
    }
}