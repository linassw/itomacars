﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ItomaCars.Models
{
    public class Segment
    {
        public int ID { get; set; }

        [StringLength(20)]
        [Display(Name = "Segment")]
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }

    public class SegmentDBContext : DbContext
    {
        public DbSet<Segment> Segments { get; set; }
    }
}