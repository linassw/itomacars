﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace ItomaCars.Models
{
    public class Status
    {
        public int ID { get; set; }
        public int? ParentID { get; set; }

        [StringLength(20)]
        public string Name { get; set; }

        public ICollection<CarStatus> CarStatuses { get; set; }

        [ForeignKey("ParentID")]
        public virtual Status Parent { get; set; }
    }

    public class StatusDBContext : DbContext
    {
        public DbSet<Status> Statuses { get; set; }
    }
}