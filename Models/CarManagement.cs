﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ItomaCars.Models
{
    public class CarManagement
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "From")]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "To")]
        public DateTime DateTo { get; set; }

        public int UserID { get; set; }
        public int SegmentID { get; set; }
        public int CarID { get; set; }

        public virtual User User { get; set; }
        public virtual Segment Segment { get; set; }
        public virtual Car Car { get; set; }
    }

    public class CarManagementDBContext : DbContext
    {
        public DbSet<CarManagement> CarManagements { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public System.Data.Entity.DbSet<ItomaCars.Models.CarDataView> CarDataViews { get; set; }
    }
}