﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ItomaCars.Models
{
    public class Car
    {
        public int ID { get; set; }

        [StringLength(20)]
        public string Number { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Year made")]
        public DateTime YearMade {get; set;}

        [StringLength(50)]
        public string Model { get; set; }

        public virtual ICollection<CarStatus> CarStatuses { get; set; }

    }

    public class CarDBContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        public System.Data.Entity.DbSet<ItomaCars.Models.User> Users { get; set; }

        public System.Data.Entity.DbSet<ItomaCars.Models.Segment> Segments { get; set; }

        public System.Data.Entity.DbSet<ItomaCars.Models.Status> Status { get; set; }

        public System.Data.Entity.DbSet<ItomaCars.Models.CarStatus> CarStatus { get; set; }

        public System.Data.Entity.DbSet<ItomaCars.Models.CarManagement> CarManagements { get; set; }
    }
}