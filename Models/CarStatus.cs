﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ItomaCars.Models
{
    public class CarStatus
    {
        public int ID { get; set; }

        public int StatusID { get; set; }
        public int CarID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "From")]
        public DateTime DateFrom { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "To")]
        public DateTime DateTo { get; set; }

        public virtual Car Car { get; set; }
        public virtual Status Status { get; set; }
    }

    public class CarStatusDBContext : DbContext
    {
        public DbSet<CarStatus> CarStatus { get; set; }
    }
}