﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ItomaCars.Models
{
    public class User
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Display(Name = "Segment")]
        public int SegmentID { get; set; }

        [Display(Name = "Segment")]
        public virtual Segment Segment { get; set; }
    }

    public class UserDBContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }
}